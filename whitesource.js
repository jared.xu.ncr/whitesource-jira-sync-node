const axios = require('axios')
const _ = require('lodash');

// module variables
const config = require('./config.json');
const productConfig = config.product;
const productFilter = productConfig.name_filter.split(",")
const projectConfig = config.project;
const projectFilter = projectConfig.name_filter.split(",")

/* --- 09/04/2020 Jared: From Whitesource API v1.1, it requires to enforce user level access and provide a userKey in json -----*/
var options = {
    url: "https://saas.whitesourcesoftware.com/api/v1.0",
    method: "POST",
    headers: {
        "content-type": "application/json",
        },
    data: {
        "requestType": 'command2override',
        "orgToken": 'ba8ead6f-1017-466c-a1cd-4ff3a7717617'
        }
};

async function getAllProductsRaw () {
    
    options.data.requestType = 'getAllProducts'
    return await axios(options).then((response) => {
        products = response.data.products
        return response.data.products;
    }).catch((error) => {
        console.log(error);
    });
}

async function getAllProducts() {
   const products = await getAllProductsRaw();
   console.log(products);

   var productMap = products.map( function(product) {
     if (productFilter.some( filter => product.productName.toLowerCase().includes(filter) )) {
            
            var info = { 
                  "productName": product.productName,
                  "productToken": product.productToken
                }
            return info;
     } 
   });
   
   return productMap.filter(function (el) { return el != null; });
}

var options2 = {
    url: "https://saas.whitesourcesoftware.com/api/v1.0",
    method: "POST",
    headers: {
        "content-type": "application/json",
        },
    data: {
        "requestType": 'getAllProjects',
        "productToken": 'abc-def-ghij-kop'
        }
};

async function getProjects (productToken) {
    
    options2.data.requestType = 'getAllProjects'
    options2.data.productToken = productToken
    console.log(options2)
    return await axios(options2).then((response) => {
        projects = response.data.projects
        return response.data.projects;
    }).catch((error) => {
        console.log(error);
    });
}

(async function getAllproject() {
    var prodMap = await getAllProducts();
    console.log(JSON.stringify(prodMap, null, 4));
    
    for (let i=0,len=prodMap.length;i<len;i++) { 
            /** struggled a bit in getting result from array sequentially, tried Map and forEach */
            var projects = await getProjects(prodMap[i].productToken)
            console.log(`Get all projects for ${prodMap[i].productName}`)
        
            var allProjects = projects.map( function(project) {
                if (projectFilter.some( filter => project.projectName.toLowerCase().includes(filter) )) {
            
                    var info = { 
                          "projectName": project.projectName,
                          "projectToken": project.projectToken
                        }
                    return info;
             } 
            }).filter(function (el) { return el != null; });
            console.log(`${JSON.stringify(allProjects, null, 4)}`);
       }
  
})();





